# Code2file

Scans PDF files for barcodes and renames the files accordingly.

## Parameters

Scans PDF files for barcodes and renames the files accordingly.
Can be executed manually or on a regular basis, e.g., via cron jobs or a service with
a wrapper script. Be aware that ideally, an execution finishes before the next one is
triggered. Select the interval long (e.g., 30 min or bigger).
```
Syntax: code2file [-s|t|d|o|h|p|i|v]

---------------------------
- Options and parameters: -
---------------------------

s     Source path where PDF files are monitored from. Example: code2file -s '/mnt/test/'.
      Relative paths are not supported!.
t     Target path where renamed PDF files are copied to. Example as above.

d     Delete source file after a successful conversion. If parameter is missing no
      deletions happen.
o     Apply OCR on final PDFs. Ignored if parameter is missing.

p     Check for prerequisites. Prints report.
i     Install required packets.

h     Print this Help.
v     Print software version and exit.
```
