#!/bin/bash
# Copyright Sebastian Schork | 03.12.2021

#
# Helper variables for counters
#
successcounter=0
errorcounter=0
filecounter=0
nocodecounter=0
codecounter=0

#
# Colors
#
RED='\033[0;31m'
GREEN='\033[0;32m'
NC='\033[0m' # No Color

#
# HELP
#
Help()
{
   echo
   echo "Scans PDF files for barcodes and renames the files accordingly."
   echo "Can be executed manually or on a regular basis, e.g., via cron jobs or a service with"
   echo "a wrapper script. Be aware that ideally, an execution finishes before the next one is"
   echo "triggered. Select the interval long (e.g., 30 min or bigger)."
   echo
   echo "Syntax: code2file [-s|t|d|o|h|p|i|v]"
   echo
   echo "---------------------------"
   echo "- Options and parameters: -"
   echo "---------------------------"
   echo
   echo "s     Source path where PDF files are monitored from. Example: code2file -s '/mnt/test/'."
   echo "      Relative paths are not supported!".
   echo "t     Target path where renamed PDF files are copied to. Example as above."
   echo
   echo "d     Delete source file after a successful conversion. If parameter is missing no"
   echo "      deletions happen."
   echo "o     Apply OCR on final PDFs. Ignored if parameter is missing."
   echo
   echo "p     Check for prerequisites. Prints report."
   echo "i     Install required packets."
   echo
   echo "h     Print this Help."
   echo "v     Print software version and exit."
   echo
}

#
# VERSION
#
Version()
{
   echo "v1.0.1"
}

#
# Check for required software
#
Preqs()
{
   echo
   echo "Prerequisites check result:"
   echo "---------------------------"

   cmd_convert=$(convert --version)
   cmd_zbarimg=$(zbarimg --version)
   cmd_ocrmypdf=$(ocrmypdf --version)

   if ! command -v convert &> /dev/null
   then
      printf "ImageMagick:    ${RED}Not OK${NC} -> check if installed.\n"
   else
      printf "ImageMagick:    ${GREEN}OK${NC}\n"
   fi
   
   if ! command -v zbarimg &> /dev/null
   then
      printf "zbarimg:        ${RED}Not OK${NC} -> check if installed.\n"
   else
      printf "zbarimg:        ${GREEN}OK${NC} -> version $cmd_zbarimg\n"
   fi

   if ! command -v ocrmypdf &> /dev/null
   then
      printf "ocrmypdf:       ${RED}Not OK${NC} -> check if installed.\n"
   else
      printf "ocrmypdf:       ${GREEN}OK${NC} -> version $cmd_ocrmypdf\n"
   fi

   echo
}

#
# Check validity of parameters
#
CheckParams()
{
   if [ -z "$source" ]
   then
         printf "${RED}Error:${NC} source parameter 's' is empty\n"
         exit
   else
         echo "Source: $source"
   fi

   if [ -z "$target" ]
   then
         printf "${RED}Error:${NC} target parameter 't' is empty\n"
         exit
   else
         echo "Target: $target"
   fi
   echo
}

#
# Install required packets.
#
Install()
{
   echo "Instaling required 'zbar' packets. Enter root password if prompted."
   sudo apt install python-zbar
   sudo apt install zbar-tools

   echo "Installing imagemagick"
   sudo apt install imagemagick
   
   echo "Installing OcrMyPDF."
   sudo apt install ocrmypdf
}

#
# Main
#
while getopts ":hvpdois:t:" option; do
   case $option in
      h) # Help
         Help
         exit;;
      v) # Version
         Version
         exit;;
      p) # Prerequisites
         Preqs
         exit;;
      i) # Install prerequisites
         Install
         exit;;
      d) echo "Warning: Deletion active"
         delete="yes";;
      o) echo "Applying OCR on each file"
         ocr="yes";;   
      s) source=${OPTARG};;
      t) target=${OPTARG};;
      \?) # Invalid option
         printf "${RED}Error:${NC} Invalid option. Run with '-h' for help.\n"
         exit;;
   esac
done

#
# Start
#
start=`date +%s`

echo "code2file started.."

#
# Error checks
#
CheckParams

#
# Monitor source and perform code scans
#
for f in "$source"*.pdf; do
   echo "------------------------------------------------------------"
   echo "Found file: $f"
   filecounter=$((filecounter+1))

   tmp="$(mktemp).jpg"
   echo "Converting content of first page to temporary JPG($tmp).."

   convert -density 250 "${f}[0]" -quality 150 "$tmp"

   echo "Analyzing converted file $tmp.."
   result=$(zbarimg -q --raw $tmp)
   result="${result//'%O'/_}"

   if [ -z "$result" ]
   then
         printf "${RED}Error:${NC} no code found in $f. Check document quality.\n"
         nocodecounter=$((nocodecounter+1))

         # Determine filename
         filename="${f##*/}"

         # create file anyway with error- prefix
         echo "Writing ERROR file $target""error-$filename"
         cp "$f" "$target""error-$filename"

         # Delete if wanted
         if [ -z "$delete" ]; then
            echo "Skipping deletion of ERROR file."
         else
            echo "Deleting original file without detected code $f.."
            rm "$f"
         fi
   else
         echo "Found code in $f: $result"
         codecounter=$((codecounter+1))

         # Perform OCR if wanted
         if [ -z "$ocr" ]; then
            echo "Skipping OCR."
         else
            echo "Performing OCR on file $target$result.pdf.."
            ocrmypdf "$f" "$f"
         fi

         # Move and rename file
         echo "Writing renamed file $target$result.pdf"
         cp "$f" "$target$result.pdf"

         # Validate and delete if wanted
         if test -f "$target$result.pdf"; then
            echo "Conversion successful."
            successcounter=$((successcounter+1))
            if [ -z "$delete" ]; then
               echo "Skipping deletion."
            else
               echo "Deleting original file $f.."
               rm "$f"
            fi
         else
            printf "${RED}Error${NC} during conversion.\n"
            errorcounter=$((errorcounter+1))
         fi         
   fi
   echo "------------------------------------------------------------"
   echo
done

# Stop measuring execution time
end=`date +%s`
runtime=$((end-start))

# Print statistics
echo
echo "Finished in $runtime seconds."
echo
printf "Total files:         $filecounter\n"
printf "Codes detected:      $codecounter\n"
printf "Codes NOT detected:  $nocodecounter\n"
printf "Conversion success:  ${GREEN}$successcounter${NC}\n"
printf "Conversion errors:   ${RED}$errorcounter${NC}\n"
echo